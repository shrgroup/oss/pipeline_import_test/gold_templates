
# SEE THIS FOR HELP

https://docs.gitlab.com/ee/ci/yaml/includes.html

`master.yml`
: Conditional Logic for which mandatory or optional childen to included

`optional.yml`
: Contains Pipelines/Stages that will be executed if enabled in project variables

`mandatory.yml`
: Contains Pipelines/Stages **ALL** projects **MUST** complete
